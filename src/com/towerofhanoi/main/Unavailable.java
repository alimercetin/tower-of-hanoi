package com.towerofhanoi.main;

public class Unavailable implements DiskState {

	@Override
	public DiskState setAvailable() {
		return new Available();
	}

	@Override
	public DiskState setFinalized() {
		return new Finalized();
	}

	@Override
	public DiskState setUnavailable() {
		return new Unavailable();
	}

	@Override
	public boolean getAvailability() {
		return false;
	}
	
	@Override
	public boolean isFinalized() {
		return false;
	}

	@Override
	public void makeMove(Disk ownerDisk, Rod destinationRod) {

	}

}