package com.towerofhanoi.main;

public class Available implements DiskState {

	@Override
	public DiskState setAvailable() {
		return new Available();
	}

	@Override
	public DiskState setFinalized() {
		return new Finalized();
	}

	@Override
	public DiskState setUnavailable() {
		return new Unavailable();
	}

	@Override
	public boolean getAvailability() {
		return true;
	}
	
	@Override
	public boolean isFinalized() {
		return false;
	}

	@Override
	public void makeMove(Disk ownerDisk, Rod destinationRod) {
		if (destinationRod == null) {
			return;
		}
		Rod previousRod = ownerDisk.getCurrentRod();
		previousRod.setToppestDisk(ownerDisk.getNextDisk());
		if(ownerDisk.getNextDisk() != null){
			ownerDisk.getNextDisk().setAvailable();
		}
		ownerDisk.setCurrentRod(destinationRod);
		ownerDisk.setNextDisk(destinationRod.getToppestDisk());
		if(destinationRod.getToppestDisk() != null){
			destinationRod.getToppestDisk().setUnavailable();
		}
		destinationRod.setToppestDisk(ownerDisk);
		String output = String.format(OUTPUT_FORMAT, previousRod.getIndex(), destinationRod.getIndex());
		System.out.println(output);
	}

}