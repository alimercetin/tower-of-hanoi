package com.towerofhanoi.main;

public class Disk {
	private Rod currentRod;
	private Disk nextDisk;
	private int index;
	private DiskState state;

	public Disk(int index) {
		this.index = index;
		this.state = new Unavailable();
	}

	public void setFinalized() {
		this.state = this.state.setFinalized();
	}

	public void setAvailable() {
		this.state = this.state.setAvailable();
	}

	public void setUnavailable() {
		this.state = this.state.setUnavailable();
	}

	public boolean getAvailability() {
		return this.state.getAvailability();
	}
	
	public boolean isFinalized() {
		return this.state.isFinalized();
	}

	public Rod getCurrentRod() {
		return currentRod;
	}

	public void setCurrentRod(Rod currentRod) {
		this.currentRod = currentRod;
	}

	public Disk getNextDisk() {
		return nextDisk;
	}

	public void setNextDisk(Disk nextDisk) {
		this.nextDisk = nextDisk;
	}

	public int getIndex() {
		return index;
	}
	
	public void makeMove(Rod destinationRod){
		this.state.makeMove(this, destinationRod);
	}

}