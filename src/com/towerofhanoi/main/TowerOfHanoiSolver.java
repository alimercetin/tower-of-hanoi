package com.towerofhanoi.main;

public class TowerOfHanoiSolver {

	public static void main(String[] args) {
		hanoiIterative(3);
	}

	private static void hanoiIterative(final int n) {
		HanoiTower solvingArea = new HanoiTower(n);
		solvingArea.solveProblem();
	}

}