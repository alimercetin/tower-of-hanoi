package com.towerofhanoi.main;

public interface DiskState {
	public static final String OUTPUT_FORMAT = "Move from %d to %d";
	
	public DiskState setAvailable();

	public DiskState setFinalized();

	public DiskState setUnavailable();

	public boolean getAvailability();

	public boolean isFinalized();

	public void makeMove(Disk ownerDisk, Rod destinationRod);
}