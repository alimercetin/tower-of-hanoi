package com.towerofhanoi.main;

import java.util.ArrayList;
import java.util.List;

public class HanoiTower {

	private static final int NUMBER_OF_RODS = 3;
	private static final int INITIAL_ROD = 1;
	private static final int FINAL_DESTINATION_INDEX = 3;
	private List<Disk> diskList;
	private List<Rod> rodList;
	private int numberOfDisks;
	private boolean isFirstMoveOfIteration = true;

	public HanoiTower(int numberOfDisks) {
		this.numberOfDisks = numberOfDisks;
		initializeRodList();
		initializeDiskList();
		placeDisks();
	}

	private void initializeRodList() {
		rodList = new ArrayList<Rod>();
		for (int i = 1; i <= NUMBER_OF_RODS; i++) {
			Rod rod = new Rod(i);
			rodList.add(rod);
		}
	}

	private void initializeDiskList() {
		diskList = new ArrayList<Disk>();
		for (int i = 1; i <= this.numberOfDisks; i++) {
			Disk disk = new Disk(i);
			diskList.add(disk);
		}
		diskList.get(0).setAvailable();
	}

	private void placeDisks() {
		Rod initialRod = rodList.get(INITIAL_ROD - 1);
		for (int i = diskList.size() - 1; i >= 0; i--) {
			Disk currentDisk = diskList.get(i);
			currentDisk.setNextDisk(initialRod.getToppestDisk());
			currentDisk.setCurrentRod(initialRod);
			initialRod.setToppestDisk(currentDisk);
		}
	}

	public void solveProblem() {
		Rod destinationRod = rodList.get(FINAL_DESTINATION_INDEX - 1);
		for (int i = numberOfDisks - 1; i >= 0; i--) {
			isFirstMoveOfIteration = true;
			Disk targetDisk = diskList.get(i);
			while (targetDisk.getCurrentRod().getToppestDisk() != targetDisk || (destinationRod.getToppestDisk() != null
					&& targetDisk.getIndex() > destinationRod.getToppestDisk().getIndex())) {
				for (int j = 0; j < i; j++) {
					Disk currentDisk = diskList.get(j);
					Rod nextRod = decideNextRodForDisk(currentDisk, i + 1);
					isFirstMoveOfIteration = false;
					currentDisk.makeMove(nextRod);
				}
			}
			targetDisk.makeMove(destinationRod);
			targetDisk.setFinalized();
		}
	}

	private Rod decideNextRodForDisk(Disk currentDisk, int numberOfDiskToMove) {
		if (!currentDisk.getAvailability()) {
			return null;
		}

		Disk nextDisk = diskList.get(currentDisk.getIndex());
		if (nextDisk.getAvailability() && currentDisk.getCurrentRod() != nextDisk.getCurrentRod()) {
			return nextDisk.getCurrentRod();
		}

		for (int i = 0; i < NUMBER_OF_RODS; i++) {
			Rod candidateRod = rodList.get(i);
			if (isFirstMoveOfIteration) {
				if (candidateRod.checkAvailabilityForDisk(currentDisk, numberOfDiskToMove, NUMBER_OF_RODS,
						FINAL_DESTINATION_INDEX)) {
					isFirstMoveOfIteration = false;
					return candidateRod;
				}
			} else {
				if (candidateRod.checkAvailabilityForDisk(currentDisk)) {
					return candidateRod;
				}
			}
		}
		return null;
	}
}