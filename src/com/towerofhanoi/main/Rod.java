package com.towerofhanoi.main;

public class Rod {
	private Disk toppestDisk;
	private int index;

	public Rod(int index) {
		this.index = index;
	}

	public boolean checkAvailabilityForDisk(Disk currentDisk) {
		if (getToppestDisk() == null) {
			return true;
		}
		if (this == currentDisk.getCurrentRod()) {
			return false;
		}
		if (getToppestDisk().getIndex() < currentDisk.getIndex()) {
			return false;
		}
		if ((getToppestDisk().getIndex() - currentDisk.getIndex()) % 2 == 1) {
			return true;
		}
		return false;

	}

	public boolean checkAvailabilityForDisk(Disk currentDisk, int numberOfDiskToMove, int numberOfRods,
			int finalDestinationIndex) {
		if (numberOfDiskToMove % 2 == 0) {
			if (this != currentDisk.getCurrentRod() && this.index != finalDestinationIndex) {
				return true;
			}
		} else {
			if (this.index == finalDestinationIndex) {
				return true;
			}
		}
		return false;
	}

	public Disk getToppestDisk() {
		return toppestDisk;
	}

	public void setToppestDisk(Disk toppestDisk) {
		this.toppestDisk = toppestDisk;
	}

	public int getIndex() {
		return index;
	}
}