package com.towerofhanoi.main;

public class Finalized implements DiskState {

	@Override
	public DiskState setAvailable() {
		return new Finalized();
	}

	@Override
	public DiskState setFinalized() {
		return new Finalized();
	}

	@Override
	public DiskState setUnavailable() {
		return new Finalized();
	}

	@Override
	public boolean getAvailability() {
		return false;
	}
	
	@Override
	public boolean isFinalized() {
		return true;
	}

	@Override
	public void makeMove(Disk ownerDisk, Rod destinationRod) {

	}

}